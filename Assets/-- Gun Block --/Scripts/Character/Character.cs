using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator), typeof(CapsuleCollider), typeof(Rigidbody))]
public class Character : MonoBehaviour
{

    //isinya
    // 1. Health
    // 2. method gerak
    // 3. handle animasi
    #region SerialzeField Variable
    [SerializeField] protected LayerMask groundMask;
    [SerializeField] protected float movementSpeed = 100f;
    [SerializeField] protected float damping = 3f;
    #endregion

    private Rigidbody rb;
    private Animator animator;

    public static event Action OnAttack;


    // Start is called before the first frame update
    protected virtual void Start()
    {
        rb = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    protected virtual void Update()
    {

    }

    protected virtual void FixedUpdate()
    {

    }

    protected virtual void Movement(float inputHorizontal, float inputVertical, float speed)
    {
        rb.velocity = (transform.forward * inputVertical * speed * Time.deltaTime) + (transform.right * inputHorizontal * speed * Time.deltaTime);
    }

    protected virtual void Attack(bool isAttacking)
    {
        if (isAttacking)
            OnAttack?.Invoke();
    }

    protected virtual void RotateTo(Transform thisObject, Vector3 target, Camera camera)
    {
        target.z = camera.nearClipPlane;
        Ray ray = camera.ScreenPointToRay(target);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, Mathf.Infinity, groundMask))
        {
            Debug.DrawLine(camera.gameObject.transform.position, hit.point, Color.yellow);

            var lookPos = hit.point - thisObject.position;
            lookPos.y = 0;
            var rotation = Quaternion.LookRotation(lookPos);
            thisObject.rotation = Quaternion.Slerp(thisObject.rotation, rotation, Time.deltaTime * damping);

        }

    }

    protected virtual void RotateTo()
    {

    }


}
