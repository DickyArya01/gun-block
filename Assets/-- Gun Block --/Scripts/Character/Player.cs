using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Character 
{
    // isinya 
    // menjalankan player berdasarkan input 

    #region Variable
    private float _inputHorizontal, _inputVertical;
    private bool _inputLeftMouse;
    private Vector3 _mousePosition;
    private Camera cam;
    #endregion

    protected override void Start()
    {
        base.Start();


        cam = Camera.main;
    }


    protected override void Update()
    {
        base.Update();


        _inputHorizontal = Input.GetAxis("Horizontal");
        _inputVertical = Input.GetAxis("Vertical");
        _inputLeftMouse = Input.GetMouseButton(0);
        _mousePosition = Input.mousePosition;


        Attack(_inputLeftMouse);

        // Debug.Log($"");

    }


    protected override void FixedUpdate()
    {
        base.FixedUpdate();

        Movement(_inputHorizontal, _inputVertical, movementSpeed);
        RotateTo(this.transform, _mousePosition, cam);

        
    }
}
