using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ObjectPool 
{
    public PoolObjectType type;
    public int amount;
    public GameObject prefab;

    public List<GameObject> pool = new List<GameObject>();
}
