using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPoolManager : MonoBehaviour, IService
{
    [SerializeField] private List<ObjectPool> listOfPool;
    [SerializeField] private List<GameObject> pool;

    private ObjectSpawner objectSpawner;

    public void Initilize()
    {
        CheckObjectType(listOfPool);
    }

    private void CheckObjectType(List<ObjectPool> listOfPool)
    {
        for (int i = 0; i < listOfPool.Count; i++)
            FillPool(listOfPool[i]);
    }

    private void FillPool(ObjectPool objectPool)
    {
        for (int i = 0; i < objectPool.amount; i++)
        {
           GameObject poolObjectInstance = Instantiate(objectPool.prefab);
           poolObjectInstance.transform.parent = transform;
           poolObjectInstance.SetActive(false);
           objectPool.pool.Add(poolObjectInstance); 
        }
    }

    public GameObject GetPoolObject(PoolObjectType type)
    {
        ObjectPool objectOfType = GetObjectByType(type);
        pool = objectOfType.pool;

        GameObject go = null;

        if (pool.Count > 0)
        {
            go = pool[pool.Count - 1];
            pool.Remove(go);
        }
        else
        {
            go = Instantiate(objectOfType.prefab);
            go.transform.parent = transform;
        }

        return go;
    }

    private ObjectPool GetObjectByType(PoolObjectType type)
    {
        for (int i = 0; i < listOfPool.Count; i++)
        {
           if (type == listOfPool[i].type)
           {
            return listOfPool[i]; 
           } 
        }
        return null;
    }

    public void CoolObject(GameObject gameObject, PoolObjectType type)
    {
        gameObject.SetActive(false);

        ObjectPool objectPool = GetObjectByType(type);
        List<GameObject> pool = objectPool.pool;

        if (!pool.Contains(gameObject))
            pool.Add(gameObject);
    }
}

