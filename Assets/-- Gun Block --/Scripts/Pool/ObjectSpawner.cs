using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawner : IService
{
    ObjectPoolManager pool;

    public bool isSpawned = false;

    public void Initilize()
    {
        pool = ServiceLocator.Current.Get<ObjectPoolManager>();
        pool.Initilize();
        Debug.Log(pool.name);
    }

    public void SpawnObject(PoolObjectType type, Vector3 pos, Quaternion rot)
    {
        GameObject go = pool.GetPoolObject(type);

        go.transform.position = pos;
        go.transform.rotation = rot;
        go.SetActive(true);
    }

}
