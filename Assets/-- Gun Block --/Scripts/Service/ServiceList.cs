using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (fileName ="Service List", menuName = "Utility/Service List")]
public class ServiceList : ScriptableObject 
{
    public ObjectPoolManager poolManager;
}

public interface IService
{
    void Initilize();
}
