using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServiceLocator
{
    private static ServiceLocator _current;

    public static ServiceLocator Current { 
        get
        {
            if (_current == null)
            {
               _current = new ServiceLocator(); 
            }

            return _current;
        }
    }

    Dictionary<string, IService> _services = new Dictionary<string, IService>();

    public ServiceLocator()
    {
        Initialize();
    }

    private void Initialize()
    {
        var serviceList = Resources.Load<ServiceList>("Service/Service List");

        ObjectPoolManager objectPoolManager = GameObject.Instantiate(serviceList.poolManager);

        Register(objectPoolManager);
        Register(new ObjectSpawner());
    }

    public void Register<T>(T service) where T : IService
    {
        if (!_services.ContainsKey(typeof(T).Name))
        {
            _services.Add(typeof(T).Name, service);
        }
    }

    public T Get<T>() where T : IService
    {
        return (T) _services[typeof(T).Name];
    }

}
