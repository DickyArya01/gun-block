using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    private ObjectSpawner objectSpawner;

    [SerializeField] private GameObject bulletPrefab;
    [SerializeField] private Transform gunPoint;
    [SerializeField] private float _delay = 0.2f;
    private float _nextShootTime;

    public List<GameObject> _bulletPool = new List<GameObject>();
    public int amountToPool = 5;

    void OnEnable()
    {
        Character.OnAttack += Attack;
    }

    void OnDisable()
    {
        Character.OnAttack -= Attack;
    }

    void Start()
    {
        objectSpawner = ServiceLocator.Current.Get<ObjectSpawner>();
        objectSpawner.Initilize();
    }

    void Attack()
    {
        if (CanShoot())
        {
            SpawnBullet();
        }
    }

    private bool CanShoot()
    {
        return Time.time >= _nextShootTime;
    }

    void SpawnBullet()
    {
        _nextShootTime = Time.time + _delay;
        objectSpawner.SpawnObject(PoolObjectType.Bullet, gunPoint.position, gunPoint.rotation);
    }

}
