using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Projectile : MonoBehaviour
{

    [SerializeField] private float bulletSpeed;
    [SerializeField] private PoolObjectType type;
    

    private Rigidbody rb;
    private ObjectPoolManager objectPoolManager;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        objectPoolManager = ServiceLocator.Current.Get<ObjectPoolManager>();
    }

    void FixedUpdate()
    {
        rb.velocity = transform.forward * bulletSpeed;
    }

    void OnBecameInvisible()
    {
        objectPoolManager.CoolObject(this.gameObject, type);
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Projectile") return;
        
        
        objectPoolManager.CoolObject(this.gameObject, type);
        Debug.Log("Kena");
    }


}
